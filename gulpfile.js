// Defining base pathes
var basePaths = {
    bower: './bower_components/'
};

var jsFileList = [
        //'src/js/jquery/jquery.js',
        'src/js/plugin/*.js',
        'src/js/custom/*.js'
    ];

var cssFileList = [
        'src/css/plugin/*.css'
    ];

// Defining requirements
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var minifyCSS = require('gulp-cssnano');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var merge2 = require('merge2');
var ignore = require('gulp-ignore');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');


// Run: 
// gulp sass
// Compiles SCSS files in CSS
gulp.task('sass', function () {
    gulp.src('./src/sass/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});


// Run: 
// gulp concat-css
// concat css file from src
gulp.task('concat-css', function() {
    return gulp.src(cssFileList)
        .pipe(plumber())
        .pipe(concat('plugin.css'))
        .pipe(gulp.dest('./css'));
});

// Run: 
// gulp concat-js
// concat js file from src
gulp.task('concat-js', function() {
    return gulp.src(jsFileList)
        .pipe(plumber())
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./js'));
});


// Run: 
// gulp minifycss
// Minifies CSS files
gulp.task('minifycss',  function(){
  return gulp.src(['./css/plugin.css', './css/style.css'])
    .pipe(plumber())
    .pipe(concat('style.min.css'))
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest('./css/'));
}); 

// Run: 
// gulp minifyjs
// Minifies js files
gulp.task('minifyjs', function() {
    return gulp.src('./js/script.js')
        .pipe(plumber())
        //.pipe(concat('script.min.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify({keepBreaks:false}))
        .pipe(gulp.dest('./js/'));
});

// Run: 
// gulp clean
// Remove file
gulp.task('clean', function () {
  return del([
    'css/*',
    // here we use a globbing pattern to match everything inside the `mobile` folder
    'js/*.js',
    // we don't want to clean this file though so we negate the pattern
    '!css/*.min.css',
    '!js/*.min.js'
  ]);
});

// Run: 
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch(cssFileList,['concat-css']);
    gulp.watch(jsFileList,['concat-js']);
});

// Run: 
// gulp copy-assets. 
// Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update

// Copy all Bootstrap JS files 
gulp.task('copy-assets', function() {
    gulp.src(basePaths.bower + 'bootstrap-sass/assets/javascripts/bootstrap.js')
       .pipe(gulp.dest('./src/js/plugin'));

// Copy all Bootstrap SCSS files
    gulp.src(basePaths.bower + 'bootstrap-sass/assets/stylesheets/**/*.scss')
       .pipe(gulp.dest('./src/sass/bootstrap-sass'));

// Copy bootstrap variable file
    gulp.src(basePaths.bower + 'bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss')
       .pipe(gulp.dest('./src/sass/theme'));

// Copy all Bootstrap Fonts
    gulp.src(basePaths.bower + 'bootstrap-sass/assets/fonts/bootstrap/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest('./fonts'));

// Copy jQuery
    gulp.src(basePaths.bower + 'jquery/dist/*.js')
        .pipe(gulp.dest('./src/js/jquery'));

// _s JS files
    gulp.src(basePaths.bower + '_s/js/*.js')
        .pipe(gulp.dest('./src/js/plugin'));
});


//task register
gulp.task('default', ['watch', 'sass', 'concat-css', 'concat-js']);
gulp.task('build', ['sass', 'concat-css', 'concat-js', 'minifycss', 'minifyjs']);
